class Event < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_rich_text :description
  has_many :pages
  has_many :rooms
  has_many :slots
  has_many :sessions
  has_many :users, through: :sessions
  has_many :tracks

  def unschedule!
    sessions.each do |s|
      s.slots = []
      s.rooms = []
      s.save
    end
  end
end
