class CreateRoomsSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :rooms_sessions do |t|
      t.references :session, null: false, foreign_key: true
      t.references :room, null: false, foreign_key: true

      t.timestamps
    end
  end
end
