class UsersController < ApplicationController
  before_action :set_event, except: [:all, :delete]
  before_action :set_user, only: [:show, :edit, :update]
  before_action :check_for_admin, except: [:index, :show]
  
  def index
    @users = @event.users.order(:last_name, :first_name).uniq.keep_if { |u| u.sessions.where(approved: true).count > 0 }
  end

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to event_users_path(@event), notice: 'Created presenter'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    if @user.update(user_params)
      redirect_to event_users_path(@event), notice: 'Updated presenter'
    else
      render :edit
    end
  end

  def master
    @users = @event.users.order(:last_name, :first_name).uniq
    render layout: 'full'
  end

  def all
    @users = User.all
  end

  def delete
    @user = User.find(params[:id])
    @user.delete
    redirect_to users_path
  end

  private

  def set_user
    @user = @event.users.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :profile)
  end
end
