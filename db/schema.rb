# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2024_09_24_020807) do

  create_table "action_text_rich_texts", force: :cascade do |t|
    t.string "name", null: false
    t.text "body"
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["record_type", "record_id", "name"], name: "index_action_text_rich_texts_uniqueness", unique: true
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "assignments", force: :cascade do |t|
    t.integer "track_id", null: false
    t.integer "room_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["room_id"], name: "index_assignments_on_room_id"
    t.index ["track_id"], name: "index_assignments_on_track_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "archived", default: "f"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "open", default: false
    t.boolean "published", default: false
    t.boolean "scheduled", default: false
    t.boolean "shared", default: false
    t.index ["slug"], name: "index_events_on_slug", unique: true
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string "slug", null: false
    t.integer "sluggable_id", null: false
    t.string "sluggable_type", limit: 50
    t.string "scope"
    t.datetime "created_at"
    t.index ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true
    t.index ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type"
    t.index ["sluggable_type", "sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_type_and_sluggable_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "title"
    t.string "slug"
    t.text "content"
    t.integer "event_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["event_id"], name: "index_pages_on_event_id"
    t.index ["slug"], name: "index_pages_on_slug", unique: true
  end

  create_table "rooms", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "capacity"
    t.integer "event_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "embed_code"
    t.index ["event_id"], name: "index_rooms_on_event_id"
  end

  create_table "rooms_sessions", force: :cascade do |t|
    t.integer "session_id", null: false
    t.integer "room_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["room_id"], name: "index_rooms_sessions_on_room_id"
    t.index ["session_id"], name: "index_rooms_sessions_on_session_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.integer "event_id", null: false
    t.string "title"
    t.text "description"
    t.text "organizer_notes"
    t.boolean "approved"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "presenter_override"
    t.index ["event_id"], name: "index_sessions_on_event_id"
  end

  create_table "sessions_slots", force: :cascade do |t|
    t.integer "session_id", null: false
    t.integer "slot_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_sessions_slots_on_session_id"
    t.index ["slot_id"], name: "index_sessions_slots_on_slot_id"
  end

  create_table "sessions_users", force: :cascade do |t|
    t.integer "session_id", null: false
    t.integer "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_sessions_users_on_session_id"
    t.index ["user_id"], name: "index_sessions_users_on_user_id"
  end

  create_table "slots", force: :cascade do |t|
    t.datetime "starts_at"
    t.datetime "ends_at"
    t.integer "event_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["event_id"], name: "index_slots_on_event_id"
  end

  create_table "taggings", force: :cascade do |t|
    t.integer "track_id", null: false
    t.integer "session_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["session_id"], name: "index_taggings_on_session_id"
    t.index ["track_id"], name: "index_taggings_on_track_id"
  end

  create_table "tracks", force: :cascade do |t|
    t.string "label"
    t.string "slug"
    t.string "color"
    t.text "description"
    t.integer "event_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["event_id"], name: "index_tracks_on_event_id"
    t.index ["slug"], name: "index_tracks_on_slug", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "administrator", default: false
    t.string "first_name"
    t.string "last_name"
    t.text "profile"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "assignments", "rooms"
  add_foreign_key "assignments", "tracks"
  add_foreign_key "pages", "events"
  add_foreign_key "rooms", "events"
  add_foreign_key "rooms_sessions", "rooms"
  add_foreign_key "rooms_sessions", "sessions"
  add_foreign_key "sessions", "events"
  add_foreign_key "sessions_slots", "sessions"
  add_foreign_key "sessions_slots", "slots"
  add_foreign_key "sessions_users", "sessions"
  add_foreign_key "sessions_users", "users"
  add_foreign_key "slots", "events"
  add_foreign_key "taggings", "sessions"
  add_foreign_key "taggings", "tracks"
  add_foreign_key "tracks", "events"
end
