class CreateRooms < ActiveRecord::Migration[6.0]
  def change
    create_table :rooms do |t|
      t.string :name
      t.text :description
      t.integer :capacity
      t.references :event, null: false, foreign_key: true

      t.timestamps
    end
  end
end
