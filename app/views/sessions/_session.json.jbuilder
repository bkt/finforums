json.extract! session, :id, :event_id, :title, :description, :organizer_notes, :approved, :created_at, :updated_at
json.url session_url(session, format: :json)
