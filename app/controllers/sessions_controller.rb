class SessionsController < ApplicationController
  before_action :set_event
  before_action :set_session, only: [:show, :edit, :update, :destroy, :approve, :revoke]
  before_action :check_for_admin, only: [:approve, :revoke]

  # GET /sessions
  # GET /sessions.json
  def index
    @sessions = if @event.scheduled || @event.published || admin?
      if admin?
        if params[:unapproved]
          @event.sessions.where(approved: false).order(:title)
        elsif params[:unfinished]
          @event.sessions.where(approved: true).order(:title).to_a.reject {|s| s.slots.count > 0 && s.rooms.count > 0 && s.users.count > 0 }
        elsif params[:finished]
          @event.sessions.where(approved: true).order(:title).to_a.keep_if {|s| s.slots.count > 0 && s.rooms.count > 0 && s.users.count > 0 }
        else
          @event.sessions.all.order(:title)
        end
      else
        @event.sessions.where(approved: true).order(:title)
      end
    else
      if current_user
        @event.sessions & current_user.sessions
      else
        redirect_to @event, error: "The public session list hasn't been published yet."
      end
    end
  end

  # GET /sessions/1
  # GET /sessions/1.json
  def show
  end

  # GET /sessions/new
  def new
    @session = @event.sessions.build
  end

  # GET /sessions/1/edit
  def edit
  end

  # POST /sessions
  # POST /sessions.json
  def create
    @session = @event.sessions.build(session_params)
    if admin?
      # @session.approved = true # not necessarily desired behavior...
    else
      @session.approved = false
      @session.users << current_user
    end

    respond_to do |format|
      if @session.save
        format.html { redirect_to [@event, @session], notice: 'Session was successfully created.' }
        format.json { render :show, status: :created, location: @session }
      else
        format.html { render :new }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sessions/1
  # PATCH/PUT /sessions/1.json
  def update
    respond_to do |format|
      if @session.update(session_params)
        format.html { redirect_to [@event, @session], notice: 'Session was successfully updated.' }
        format.json { render :show, status: :ok, location: @session }
      else
        format.html { render :edit }
        format.json { render json: @session.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sessions/1
  # DELETE /sessions/1.json
  def destroy
    if @session.approved
      flash[:error] = 'You cannot destroy an approved session.'
    else
      @session.taggings.each(&:destroy)
      @session.destroy
      flash[:notice] = 'Session was successfully destroyed.'
    end
    respond_to do |format|
      format.html { redirect_to event_sessions_url(@event) }
      format.json { head :no_content }
    end
  end

  def approve
    @session.approved = true
    @session.save
    redirect_to [@event, @session], notice: 'Approved session.'
  end

  def revoke
    @session.approved = false
    @session.save
    redirect_to [@event, @session], notice: 'Revoked approval.'
  end

  def grid
    @sessions = if @event.scheduled || admin?
      @event.sessions.all
      render layout: 'full'
    else
      redirect_to @event, notice: 'The schedule is not ready yet'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_params
      params.require(:session).permit(:title, :description, :organizer_notes, :links, :presenter_override, slot_ids: [], room_ids: [], user_ids: [], track_ids: [])
    end
end
