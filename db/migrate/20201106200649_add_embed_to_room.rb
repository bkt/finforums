class AddEmbedToRoom < ActiveRecord::Migration[6.0]
  def change
    add_column :rooms, :embed_code, :string
  end
end
