class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  helper_method :admin?
  def admin?
    current_user && current_user.admin
  end

  def check_for_admin
    redirect_to root_path unless admin?
  end

  def set_event
    @event = Event.friendly.find(params[:event_id])
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :profile])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :profile])
  end
end
