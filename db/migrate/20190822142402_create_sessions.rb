class CreateSessions < ActiveRecord::Migration[6.0]
  def change
    create_table :sessions do |t|
      t.references :event, null: false, foreign_key: true
      t.string :title
      t.text :description
      t.text :organizer_notes
      t.boolean :approved

      t.timestamps
    end
  end
end
