class AddPresenterOverrideToSession < ActiveRecord::Migration[6.0]
  def change
    add_column :sessions, :presenter_override, :string
  end
end
