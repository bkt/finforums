class AddSharedToEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :shared, :boolean, default: false
  end
end
