class Page < ApplicationRecord
  extend FriendlyId
  friendly_id :title, use: :slugged
  has_rich_text :content
  belongs_to :event

  scope :publishable, -> { where("title NOT LIKE '_%'") }

  def should_generate_new_friendly_id?
   title_changed?
  end
end
