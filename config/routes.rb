Rails.application.routes.draw do
  devise_for :users
  get '/manage_users' => 'users#all', as: 'users'
  post '/manage_users/:id' => 'users#delete', as: 'delete_user'


  resources :events, path: '/' do
    resources :tracks
    resources :sessions do
      post 'approve', on: :member
      post 'revoke', on: :member
      get 'grid', on: :collection
    end
    resources :users, except: [:delete], path: :presenters do
      get 'master', on: :collection
    end
    resources :pages
    resources :rooms
    resources :slots
    get 'archive', on: :member
    get 'unarchive', on: :member
  end

  root to: 'events#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
