class CreateSessionsSlots < ActiveRecord::Migration[6.0]
  def change
    create_table :sessions_slots do |t|
      t.references :session, null: false, foreign_key: true
      t.references :slot, null: false, foreign_key: true

      t.timestamps
    end
  end
end
