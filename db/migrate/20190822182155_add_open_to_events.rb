class AddOpenToEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :open, :boolean, default: false
    add_column :events, :published, :boolean, default: false
    add_column :events, :scheduled, :boolean, default: false
  end
end
