class Slot < ApplicationRecord
  belongs_to :event
  has_and_belongs_to_many :sessions

  def name
    r = ''
    r << starts_at.strftime('%b %e, %l:%M %p - ')
    if starts_at.to_date == ends_at.to_date
      r << ends_at.strftime('%l:%M %p')
    else
      r << ends_at.strftime('%b %e, %l:%M %p - ')
    end
    r
  end

  def happening?
    starts_at <= Time.now && ends_at >= Time.now
  end
end
