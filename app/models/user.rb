class User < ApplicationRecord
  has_rich_text :profile
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_and_belongs_to_many :sessions

  def admin?
    administrator
  end

  alias :admin :admin?

  def name
    "#{first_name} #{last_name}"
  end
end
