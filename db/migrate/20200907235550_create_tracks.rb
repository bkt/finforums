class CreateTracks < ActiveRecord::Migration[6.0]
  def change
    create_table :tracks do |t|
      t.string :label
      t.string :slug
      t.string :color
      t.text :description
      t.references :event, null: false, foreign_key: true

      t.timestamps
    end
    add_index :tracks, :slug, unique: true
  end
end
