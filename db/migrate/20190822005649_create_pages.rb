class CreatePages < ActiveRecord::Migration[6.0]
  def change
    create_table :pages do |t|
      t.string :title
      t.string :slug
      t.text :content
      t.references :event, null: false, foreign_key: true

      t.timestamps
    end
    add_index :pages, :slug, unique: true
  end
end
