require 'test_helper'

class PresentersControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get presenters_index_url
    assert_response :success
  end

  test "should get show" do
    get presenters_show_url
    assert_response :success
  end

end
