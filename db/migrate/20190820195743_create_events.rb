class CreateEvents < ActiveRecord::Migration[6.0]
  def change
    create_table :events do |t|
      t.string :name
      t.string :slug
      t.string :archived, default: false

      t.timestamps
    end
    add_index :events, :slug, unique: true
  end
end
