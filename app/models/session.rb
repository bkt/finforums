class Session < ApplicationRecord
  has_rich_text :description
  has_rich_text :organizer_notes
  has_rich_text :links
  belongs_to :event
  has_and_belongs_to_many :slots
  has_and_belongs_to_many :rooms
  has_and_belongs_to_many :users
  has_many :taggings, dependent: :destroy
  has_many :tracks, through: :taggings

  def to_presenter_info
    "#{title} - #{slots.map(&:name).join('/')} - #{rooms.map(&:name).join('/')}"
  end

  def live?
    approved && slots.any?(&:happening?) && rooms.present?
  end
end
