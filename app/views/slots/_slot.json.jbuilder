json.extract! slot, :id, :starts_at, :ends_at, :event_id, :created_at, :updated_at
json.url slot_url(slot, format: :json)
