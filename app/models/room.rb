class Room < ApplicationRecord
  has_rich_text :description
  belongs_to :event
  has_many :assignments
  has_many :tracks, through: :assignments
  has_and_belongs_to_many :sessions
end
