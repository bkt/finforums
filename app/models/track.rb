class Track < ApplicationRecord
  extend FriendlyId
  friendly_id :label, use: :slugged
  has_many :taggings
  has_many :sessions, through: :taggings
  has_many :assignments
  has_many :rooms, through: :assignments
  belongs_to :event
  has_rich_text :description
end
